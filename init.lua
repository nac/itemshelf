-- Itemshelf mod by Zorman2000

local modpath = minetest.get_modpath("itemshelf")

-- Load files
dofile(modpath .. "/api.lua")
dofile(modpath .. "/nodes.lua")
dofile(modpath .. "/recipes.lua") --from nac (minetest4kids)
